TagTor is a flask web app to display metrics about the Tor network and its nodes.

## Development

### Installation

Git clone this repositry.

Create a [virtual environment](https://docs.python.org/3/library/venv.html)
amd activate it.

Install the python dependencies and package in the virtual
environment.

```bash
cd tagtor
pip install -e .
```

### Configuration

Create an `instance` directory if it doesn't exist already and
a `config.dev.py` file inside it. Example `config.dev.py`:

```python
SECRET_KEY = "chageme"
REMOTE = True
METRICS_DB_URL = "https://example.com/api/v1/query_range"
METRICS_DB_USER = "metricsuser"
METRICS_DB_PASSWORD = "metricspassword"
DB_HOST = "example.com:5432"
DB_USER = "dbuser"
DB_PASSWORD = "dbpassword"
DB_NAME = "dbname"
# Optional
DB_SSLMODE = "verify-full"
# To obtain the `instance` path with Python
import pathlib
THIS_PATH = pathlib.Path(__file__).parent.resolve()
DB_SSLROOTCERT = THIS_PATH / "rootcert.crt"
```

You can add environment-specific config files in the `instance`
directory. To use a specific config, export the `TAGTOR_CONFIG`
variable with the path to the desired config file.

The `./bin/run` script defaults `TAGTOR_CONFIG` to `config.dev.py`.

During startup, the app first loads `config/default_config.py`.
If `TAGTOR_CONFIG` is set, it loads that config file next,
overriding values from `default_config.py`.

### Running

Run this [flask](https://flask.palletsprojects.com/) application:

```bash
export FLASK_DEBUG=true
export TAGTOR_CONFIG=config.dev.py
flask --app tagtor run
```

or run `.bin/run`

Run a Web server proxying the application. Using the docker nginx
configuration and `.htpasswd` files in this repository:

```bash
docker run -it --rm --net=host --name nginx \
-v ./default.conf:/etc/nginx/conf.d/default.conf \
-v ./htpasswd:/etc/apache2/.htpasswd -d nginx
```

or run  `.bin/run-web`

If you need to generate an `.htpasswd` user and password, run
`htpasswd -c /path/.htpasswd myuser`

You can now browse this application with a browser at
http://localhost
