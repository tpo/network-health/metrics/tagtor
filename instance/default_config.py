TESTING = False
METRICS_DB_USER = "metrics"
METRICS_DB_PASSWORD = ""
METRICS_DB_URL = "http://127.0.0.1:8428/prometheus/api/v1/query_range"
DB_USER = "metrics"
DB_PASSWORD = "password"
DB_HOST = "postgresdb.orb.local"
DB_NAME = "metrics"
SECRET_KEY = "93132ab7234ba319a0d8361bd2f55f145d705c57abd94e94feec5ba1e798da17"
REMOTE = False

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "format": "%(asctime)s : %(levelname)s : %(message)s",
            "datefmt": "%m/%d/%Y %I:%M:%S %p",
        },
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "default",
        },
        "file": {
            "level": "WARN",
            "class": "logging.handlers.TimedRotatingFileHandler",
            "filename": "logs/app.log",
            "when": "midnight",
            "utc": True,
            "backupCount": 30,
            "formatter": "default",
        },
        "wsgi": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "stream": "ext://flask.logging.wsgi_errors_stream",
            "formatter": "default",
        },
    },
    "loggers": {
        "": {
            "level": "WARN",
            "handlers": [
                "console",
                "file",
            ],
            "propagate": True,
        },
    },
}
