from dataclasses import dataclass
from datetime import datetime
from pypika import Table


@dataclass
class ServerNoteModel:
    note_id: int
    note: str
    username: str
    published: datetime
    edited: datetime
    fingerprint: str
    status: str


server_note = Table("server_note")
