from dataclasses import dataclass
import datetime
from pypika import Table


@dataclass
class ServerTagModel:
    tag: str
    username: str
    published: datetime
    edited: datetime
    fingerprint: str
    status: str
    digest: str


server_tag = Table("server_tag")
