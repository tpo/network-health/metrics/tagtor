from dataclasses import dataclass
from datetime import datetime
from typing import Optional
from pypika import Table


@dataclass
class ServerFamiliesModel:
    digest: str
    published: datetime
    updated: datetime
    members: Optional[int] = None
    effective_family: Optional[str] = None


server_families = Table("server_families")
