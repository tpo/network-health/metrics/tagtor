from dataclasses import dataclass
from datetime import datetime
from typing import Optional
from pypika import Table


@dataclass
class ServerStatusModel:
    is_bridge: bool
    published: datetime
    nickname: str
    fingerprint: str
    last_seen: datetime
    first_seen: datetime
    last_restarted: datetime
    or_addresses: Optional[str] = None
    running: Optional[bool] = None
    flags: Optional[str] = None
    country: Optional[str] = None
    country_name: Optional[str] = None
    autonomous_system: Optional[str] = None
    as_name: Optional[str] = None
    verified_host_names: Optional[str] = None
    exit_policy: Optional[str] = None
    contact: Optional[str] = None
    platform: Optional[str] = None
    version: Optional[str] = None
    version_status: Optional[str] = None
    effective_family: Optional[str] = None
    declared_family: Optional[str] = None
    transport: Optional[str] = None
    bridgedb_distributor: Optional[str] = None
    blocklist: Optional[str] = None
    last_changed_address_or_port: Optional[datetime] = None
    diff_or_addresses: Optional[str] = None
    unverified_host_names: Optional[str] = None
    unreachable_or_addresses: Optional[str] = None
    overload_ratelimits_version: Optional[int] = None
    overload_ratelimits_timestamp: Optional[datetime] = None
    overload_ratelimits_ratelimit: Optional[int] = None
    overload_ratelimits_burstlimit: Optional[int] = None
    overload_ratelimits_read_count: Optional[int] = None
    overload_ratelimits_write_count: Optional[int] = None
    overload_fd_exhausted_version: Optional[int] = None
    overload_fd_exhausted_timestamp: Optional[datetime] = None
    overload_general_timestamp: Optional[datetime] = None
    overload_general_version: Optional[int] = None
    first_network_status_digest: Optional[str] = None
    first_network_status_entry_digest: Optional[str] = None
    last_network_status_digest: Optional[str] = None
    last_network_status_entry_digest: Optional[str] = None
    first_bridge_network_status_digest: Optional[str] = None
    first_bridge_network_status_entry_digest: Optional[str] = None
    last_bridge_network_status_digest: Optional[str] = None
    last_bridge_network_status_entry_digest: Optional[str] = None
    last_network_status_weights_id: Optional[str] = None
    last_network_status_totals_id: Optional[str] = None
    last_server_descriptor_digest: Optional[str] = None
    last_extrainfo_descriptor_digest: Optional[str] = None
    last_bridgestrap_stats_digest: Optional[str] = None
    last_bridgestrap_test_digest: Optional[str] = None
    last_bridge_pool_assignments_file_digest: Optional[str] = None
    last_bridge_pool_assignment_digest: Optional[str] = None
    family_digest: Optional[str] = None
    advertised_bandwidth: Optional[int] = None
    bandwidth: Optional[int] = None
    network_weight_fraction: Optional[float] = None
    bandwidth_avg: Optional[int] = None
    bandwidth_burst: Optional[int] = None
    bandwidth_observed: Optional[int] = None
    ipv6_default_policy: Optional[str] = None
    ipv6_port_list: Optional[str] = None
    socks_port: Optional[int] = None
    dir_port: Optional[int] = None
    or_port: Optional[int] = None
    is_hibernating: Optional[bool] = None
    address: Optional[str] = None


server_status_latest = Table("server_status_latest")
