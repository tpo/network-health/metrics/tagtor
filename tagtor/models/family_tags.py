from dataclasses import dataclass
from datetime import datetime
from pypika import Table


@dataclass
class FamilyTagsModel:
    digest: str
    tags: str
    published: datetime
    edited: datetime
    status: str


family_tags = Table("family_tags")
