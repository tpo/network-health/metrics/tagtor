"""
Database logic
"""

from contextlib import contextmanager
from dataclasses import fields
import psycopg2
import psycopg2.pool
from psycopg2.extras import RealDictCursor
from pypika import Query, Order, Criterion, functions as fn

from datetime import datetime

from flask import current_app

from tagtor.dto.families import FamiliesStatusModel
from tagtor.models.family_tags import family_tags
from tagtor.models.note import ServerNoteModel, server_note
from tagtor.models.tags import ServerTagModel, server_tag
from tagtor.models.status import ServerStatusModel, server_status_latest
from tagtor.models.families import server_families
from .constants import RouterFilters, SortDirection


class Database(object):
    """
    Connect to the postgresql database abstracting over
    psycopg2 methods
    """

    pool: psycopg2.pool.SimpleConnectionPool

    @classmethod
    def initialise(cls) -> None:
        """
        Initialise connection with database
        """
        # Dictionary to use as keyword arguments for the database connection
        db_kwargs = {
            "host": current_app.config["DB_HOST"],
            "database": current_app.config["DB_NAME"],
            "user": current_app.config["DB_USER"],
            "password": current_app.config["DB_PASSWORD"],
        }
        # Optional arguments
        if current_app.config.get("DB_SSLMODE", None):
            db_kwargs["sslmode"] = current_app.config["DB_SSLMODE"]
        if current_app.config.get("DB_SSLROOTCERT", None):
            db_kwargs["sslrootcert"] = current_app.config["DB_SSLROOTCERT"]
        # Create a connection pool
        try:
            cls.pool = psycopg2.pool.SimpleConnectionPool(1, 10, **db_kwargs)
        except Exception as err:
            current_app.logger.error(err)

    @classmethod
    @contextmanager
    def execute(cls):
        """
        Execute queries with the cursor instance
        """
        conn = None
        cursor = None
        try:
            conn = cls.pool.getconn()
            cursor = conn.cursor(cursor_factory=RealDictCursor)
            yield cursor
        except Exception as e:
            current_app.logger.error(e)
            if conn:
                conn.rollback()
            raise e
        finally:
            if cursor:
                cursor.close()
            if conn:
                conn.commit()
                cls.pool.putconn(conn)

    @classmethod
    def get_routers(
        cls, filters: RouterFilters, limit, offset, sort_by, sort_dir: SortDirection
    ) -> list[ServerStatusModel]:
        """
        Fetch routers from DB

        This method also implements a primitive search function over routers.
        This was modelled over what we are currently using on relay search.
        The difference is that the {cat}:{keyword} syntax express a database
        field and a keyword to be searched in the server_status table.

        If no category is provided, the keyword is defaulted to the nickname
        for now.
        """
        query = Query.from_(server_status_latest).select("*")
        # Filters
        for field in fields(filters):
            key = field.name
            value = getattr(filters, key)

            if value is not None:
                if key == "flags":
                    query = query.where(
                        getattr(server_status_latest, key).like("%" + value[0] + "%")
                    )
                elif key == "tagged":
                    fingerprints: list[str] = []
                    if value:
                        fingerprints = cls.get_tagged_fingerprints()
                    else:
                        fingerprints = cls.get_untagged_fingerprints()
                    if len(fingerprints) == 0:
                        fingerprints = [""]
                    query = query.where(
                        server_status_latest.fingerprint.isin(fingerprints)
                    )

                elif key == "tags":
                    fingerprints: list[str] = []
                    value = value.split(",")
                    fingerprints = cls.get_fingerprints_by_tags(value)
                    if len(fingerprints) == 0:
                        fingerprints = [""]
                    query = query.where(
                        server_status_latest.fingerprint.isin(fingerprints)
                    )
                elif type(value) is list:
                    query = query.where(getattr(server_status_latest, key).isin(value))
                elif type(value) is bool:
                    query = query.where(getattr(server_status_latest, key) == value)
                else:
                    query = query.where(
                        getattr(server_status_latest, key).like("%" + value + "%")
                    )
        # Sort
        query = query.orderby(
            getattr(server_status_latest, sort_by),
            order=Order.desc if sort_dir == "desc" else Order.asc,
        )
        # Limit
        query = query.limit(limit)
        # Offset
        query = query.offset(offset)
        routers = []
        with cls.execute() as cur:
            cur.execute(query.get_sql())
            routers = cur.fetchall()
        return [ServerStatusModel(**router) for router in routers]

    @classmethod
    def get_families(
        cls, filters: RouterFilters, limit, offset
    ) -> list[FamiliesStatusModel]:
        query = (
            Query.from_(server_status_latest)
            .select(
                server_status_latest.family_digest,
                fn.Max(server_status_latest.published).as_("published"),
                fn.Sum(server_status_latest.bandwidth).as_("bandwidth"),
                fn.Sum(server_status_latest.advertised_bandwidth).as_(
                    "advertised_bandwidth"
                ),
                fn.Sum(server_status_latest.network_weight_fraction).as_(
                    "network_weight_fraction"
                ),
                fn.Count(server_status_latest.fingerprint).as_("fingerprints"),
            )
            .groupby(server_status_latest.family_digest)
        )
        # Filters
        for field in fields(filters):
            key = field.name
            value = getattr(filters, key)

            if value is not None:
                if key == "flags":
                    query = query.where(
                        getattr(server_status_latest, key).like("%" + value[0] + "%")
                    )
                elif key == "tagged":
                    fingerprints: list[str] = []
                    if value:
                        fingerprints = cls.get_tagged_fingerprints()
                    else:
                        fingerprints = cls.get_untagged_fingerprints()
                    if len(fingerprints) == 0:
                        fingerprints = [""]
                    query = query.where(
                        server_status_latest.fingerprint.isin(fingerprints)
                    )

                elif key == "tags":
                    fingerprints: list[str] = []
                    value = value.split(",")
                    fingerprints = cls.get_fingerprints_by_tags(value)
                    if len(fingerprints) == 0:
                        fingerprints = [""]
                    query = query.where(
                        server_status_latest.fingerprint.isin(fingerprints)
                    )
                elif type(value) is list:
                    query = query.where(getattr(server_status_latest, key).isin(value))
                elif type(value) is bool:
                    query = query.where(getattr(server_status_latest, key) == value)
                else:
                    query = query.where(
                        getattr(server_status_latest, key).like("%" + value + "%")
                    )
        # Sort
        query = query.orderby(
            fn.Sum(server_status_latest.network_weight_fraction), order=Order.desc
        )
        # Limit
        query = query.limit(limit)
        # Offset
        query = query.offset(offset)
        routers = []
        with cls.execute() as cur:
            cur.execute(query.get_sql())
            routers = cur.fetchall()
        return [FamiliesStatusModel(**router) for router in routers]

    @classmethod
    def get_notes(cls, fingerprint: str) -> list[ServerNoteModel]:
        """
        Fetch all notes for given router
        """
        query = (
            Query.from_(server_note)
            .select("*")
            .where(server_note.fingerprint == fingerprint)
        )
        notes = []
        with cls.execute() as cur:
            cur.execute(query.get_sql())
            notes = cur.fetchall()
        return [ServerNoteModel(**note) for note in notes]

    @classmethod
    def get_note(cls, note_id: str) -> ServerNoteModel:
        """
        Fetch note by id for given router
        """
        query = (
            Query.from_(server_note).select("*").where(server_note.note_id == note_id)
        )
        note = None
        with cls.execute() as cur:
            cur.execute(query.get_sql())
            note = cur.fetchone()
        return ServerNoteModel(**note)

    @classmethod
    def get_notes_by_username(cls, username: str) -> list[ServerNoteModel]:
        """
        Fetch all notes for given username
        """
        query = (
            Query.from_(server_note).select("*").where(server_note.username == username)
        )
        notes = []
        with cls.execute() as cur:
            cur.execute(query.get_sql())
            notes = cur.fetchall()
        return [ServerNoteModel(**note) for note in notes]

    @classmethod
    def get_tags(cls, fingerprint: str) -> list[ServerTagModel]:
        """
        Fetch all tags for given router
        """
        query = (
            Query.from_(server_tag)
            .select("*")
            .where(server_tag.fingerprint == fingerprint)
        )
        tags = []
        with cls.execute() as cur:
            cur.execute(query.get_sql())
            tags = cur.fetchall()
        return [ServerTagModel(**tag) for tag in tags]

    @classmethod
    def get_tags_by_username(cls, username: str) -> list[ServerTagModel]:
        """
        Fetch all notes for given username
        """
        query = (
            Query.from_(server_tag).select("*").where(server_tag.username == username)
        )
        tags = []
        with cls.execute() as cur:
            cur.execute(query.get_sql())
            tags = cur.fetchall()
        return [ServerTagModel(**tag) for tag in tags]

    @classmethod
    def get_tagged_fingerprints(cls) -> list[str]:
        """
        Search for tagged fingerprint
        """
        query = (
            # family_digest is null & fingerprint is in server_tag
            Query.from_(server_status_latest)
            .select(server_status_latest.fingerprint)
            .join(server_tag)
            .on(server_status_latest.fingerprint == server_tag.fingerprint)
            .where(server_status_latest.family_digest.isnull())
            .where(server_tag.status != "deleted")
        ) + (
            # family_digest is not null & family_digest is in server_tag
            Query.from_(server_status_latest)
            .select(server_status_latest.fingerprint)
            .join(server_tag)
            .on(server_status_latest.family_digest == server_tag.digest)
            .where(server_status_latest.family_digest.notnull())
            .where(server_tag.status != "deleted")
        )
        fingerprints = []
        with cls.execute() as cur:
            cur.execute(query.get_sql())
            fingerprints = cur.fetchall()
        return [fingerprint.get("fingerprint") for fingerprint in fingerprints]

    @classmethod
    def get_untagged_fingerprints(cls) -> list[str]:
        """
        Search for untagged fingerprints
        """
        query = (
            Query.from_(server_status_latest)
            .select(server_status_latest.fingerprint)
            .where(
                Criterion.any(
                    [
                        # family_digest is null & fingerprint is not in server_tag
                        (
                            server_status_latest.family_digest.isnull()
                            & server_status_latest.fingerprint.isin(
                                Query.from_(server_tag)
                                .select(server_tag.fingerprint)
                                .where(server_tag.status != "deleted")
                            ).negate()
                        ),
                        # family_digest is not null & digest is not in server_tag
                        (
                            server_status_latest.family_digest.notnull()
                            & server_status_latest.family_digest.isin(
                                Query.from_(server_tag)
                                .select(server_tag.digest)
                                .where(server_tag.status != "deleted")
                            ).negate()
                        ),
                    ]
                )
            )
        )
        fingerprints = []
        with cls.execute() as cur:
            cur.execute(query.get_sql())
            fingerprints = cur.fetchall()
        return [fingerprint.get("fingerprint") for fingerprint in fingerprints]

    @classmethod
    def get_fingerprints_by_tags(cls, keywords: list[str]) -> list[str]:
        """
        Search fingerprints by tag
        """
        query = Query.from_(server_tag).select("*")
        if len(keywords) == 0:
            keywords = [""]
        query = query.where(server_tag.tag.isin(keywords))
        tags = []
        with cls.execute() as cur:
            cur.execute(query.get_sql())
            tags = cur.fetchall()
        return [ServerTagModel(**tag).fingerprint for tag in tags]

    @classmethod
    def get_routers_by_fingerprints(cls, fingerprints: list[str]):
        """
        Search routers by fingerprints
        """
        query = (
            Query.from_(server_status_latest)
            .select(server_status_latest.fingerprint, server_status_latest.nickname)
            .distinct()
            .orderby(
                server_status_latest.fingerprint,
                server_status_latest.nickname,
                server_status_latest.published,
                order=Order.desc,
            )
        )
        if len(fingerprints) == 0:
            fingerprints = [""]
        query = query.where(server_status_latest.fingerprint.isin(fingerprints))

        routers = []
        with cls.execute() as cur:
            cur.execute(query.get_sql())
            routers = cur.fetchall()
        return routers

    @classmethod
    def get_top_tags(cls, limit: int) -> list[ServerTagModel]:
        """
        Fetch a list of tags and their frequency
        """
        subquery = (
            Query.from_(server_tag)
            .select(server_tag.tag, fn.Count("*").as_("tag_count"))
            .groupby(server_tag.tag)
            .orderby("tag_count", order=Order.desc)
            .limit(limit)
        )
        query = (
            Query.from_(server_tag)
            .select(server_tag.star)
            .join(subquery)
            .on(server_tag.tag == subquery.tag)
        )
        tags = []
        with cls.execute() as cur:
            cur.execute(query.get_sql())
            tags = cur.fetchall()
        return [ServerTagModel(**tag) for tag in tags]

    @classmethod
    def get_router(cls, fingerprint: str) -> ServerStatusModel:
        """
        Fetch router from DB
        """
        query = (
            Query.from_(server_status_latest)
            .select("*")
            .where(server_status_latest.fingerprint == fingerprint)
            .orderby(server_status_latest.published, order=Order.desc)
            .limit(1)
        )
        router = None
        with cls.execute() as cur:
            cur.execute(query.get_sql())
            router = cur.fetchone()
        return ServerStatusModel(**router)

    @classmethod
    def get_family(cls, fingerprint: str):
        """
        Fetch router effective family from db
        """
        router = cls.get_router(fingerprint)
        family: list[str] = []
        if router and router.effective_family:
            family = router.effective_family.split(",")
            if len(family) > 1:
                family = router.effective_family[1:-1].split(",")
                family = [fp[1:-1] for fp in family]
        return family

    @classmethod
    def get_tags_by_family(cls, digest: str) -> list[ServerTagModel]:
        """
        Get family tags
        """
        query = Query.from_(server_tag).select("*").where(server_tag.digest == digest)
        tags = []
        with cls.execute() as cur:
            cur.execute(query.get_sql())
            tags = cur.fetchall()
        return [ServerTagModel(**tag) for tag in tags]

    @classmethod
    def add_tags(cls, fingerprint: str, username: str, tags: list[str], digest=""):
        """
        Add tags to router
        """
        # Insert tags into server_tag
        cls.insert_tags(fingerprint, username, tags, digest)

        # Get the family for the given fingerprint
        family = cls.get_family(fingerprint).pop()
        fingerprints = family.split(" ")
        if len(fingerprints) > 1:
            family_str = family.replace('"', "")
            query = (
                Query.from_(server_families)
                .select(server_families.digest)
                .where(server_families.effective_family == family_str)
            )
            digest = []
            with cls.execute() as cur:
                cur.execute(query.get_sql())
                digest = cur.fetchall()
            if len(digest) > 0:
                digest = digest.pop()
                digest = f"{digest}".strip("(),")
                # Update or insert the tags into family_tags based on the digest
                cls.update_or_insert_family_tags(digest, tags)

    @classmethod
    def insert_tags(
        cls, fingerprint: str, username: str, tags: list[str], digest: str
    ) -> None:
        """
        Insert multiple tags at once
        """
        published = datetime.now()
        query = Query.into(server_tag).columns(
            server_tag.tag,
            server_tag.username,
            server_tag.published,
            server_tag.edited,
            server_tag.fingerprint,
            server_tag.status,
            server_tag.digest,
        )
        for tag in tags:  # Iterate through each tag
            query = query.insert(
                tag, username, published, published, fingerprint, "published", digest
            )

        with cls.execute() as cur:
            cur.execute(query.get_sql())

    @classmethod
    def update_or_insert_family_tags(cls, digest: str, tags: list[str]) -> None:
        """
        Update or insert multiple tags into family_tags based on digest
        """
        if not tags or not digest:
            return

        with cls.execute() as cur:
            for tag in tags:
                # Check if the digest is available
                check_digest_query = (
                    Query.from_(family_tags)
                    .select("*")
                    .where(family_tags.digest == digest)
                )
                cur.execute(check_digest_query.get_sql())
                available_digests = cur.fetchall()
                is_digest_available = len(available_digests) > 0
                if not is_digest_available:
                    insert_query = (
                        Query.into(family_tags)
                        .columns(
                            family_tags.digest,
                            family_tags.tags,
                            family_tags.published,
                            family_tags.edited,
                            family_tags.status,
                        )
                        .insert(digest, tag, fn.Now(), fn.Now(), "published")
                    )
                    cur.execute(insert_query.get_sql())
                    return

                # Check if the tag already exists for the digest
                check_tag_query = (
                    Query.from_(family_tags)
                    .select("*")
                    .where(family_tags.digest == digest)
                    .where(family_tags.tags.like("%" + tag + "%"))
                )
                cur.execute(check_tag_query.get_sql())
                digests = cur.fetchall()
                if len(digests) == 0:
                    update_query = (
                        Query.update(family_tags)
                        .set(family_tags.tags, fn.Concat(family_tags.tags, ",", tag))
                        .set(family_tags.edited, fn.Now())
                        .where(family_tags.digest == digest)
                    )
                    cur.execute(update_query.get_sql())

    @classmethod
    def set_tag_status(
        cls, fingerprint: str, username: str, tag: str, status: str
    ) -> None:
        """
        Change status of tag to deleted
        """
        query = (
            Query.update(server_tag)
            .set(server_tag.status, status)
            .set(server_tag.edited, fn.Now())
            .where(server_tag.fingerprint == fingerprint)
            .where(server_tag.tag == tag)
            .where(server_tag.username == username)
        )
        with cls.execute() as cur:
            cur.execute(query.get_sql())

    @classmethod
    def add_note(cls, fingerprint: str, username: str, note: str) -> None:
        """
        Add note to router
        """
        query = (
            Query.into(server_note)
            .columns(
                server_note.note,
                server_note.username,
                server_note.published,
                server_note.edited,
                server_note.fingerprint,
                server_note.status,
            )
            .insert(note, username, fn.Now(), fn.Now(), fingerprint, "published")
        )
        with cls.execute() as cur:
            cur.execute(query.get_sql())

    @classmethod
    def edit_note(
        cls, fingerprint: str, username: str, note_id: str, note: str, status: str
    ) -> None:
        """
        Edit router note
        """
        query = (
            Query.update(server_note)
            .set(server_note.status, status)
            .set(server_note.edited, fn.Now())
            .set(server_note.note, note)
            .where(server_note.fingerprint == fingerprint)
            .where(server_note.note_id == note_id)
            .where(server_note.username == username)
        )
        with cls.execute() as cur:
            cur.execute(query.get_sql())

    @classmethod
    def delete_note(cls, fingerprint: str, username: str, note_id: str) -> None:
        """
        Delete router note
        """
        query = (
            Query.update(server_note)
            .set(server_note.status, "deleted")
            .set(server_note.edited, fn.Now())
            .where(server_note.fingerprint == fingerprint)
            .where(server_note.note_id == note_id)
            .where(server_note.username == username)
        )
        with cls.execute() as cur:
            cur.execute(query.get_sql())

    @classmethod
    def restore_note(cls, fingerprint: str, username: str, note_id: str) -> None:
        """
        Restore deleted router note
        """
        query = (
            Query.update(server_note)
            .set(server_note.status, "published")
            .set(server_note.edited, fn.Now())
            .where(server_note.fingerprint == fingerprint)
            .where(server_note.note_id == note_id)
            .where(server_note.username == username)
        )
        with cls.execute() as cur:
            cur.execute(query.get_sql())
