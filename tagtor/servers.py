import dataclasses
from flask import Blueprint, current_app, render_template

from tagtor.auth import current_username
from tagtor.chart import Chart
from tagtor.dto.servers import ServersResponseDTO
from tagtor.vm import VictoriaMetrics
from datetime import datetime

bp = Blueprint("servers", __name__)


@bp.route("/servers")
def index():
    vm = VictoriaMetrics()

    if current_app.config["REMOTE"]:
        vm.authenticate()

    params_relays = {"query": "sum_over_time(count(network_fraction) by (day))[24h]"}
    time_vector_relays = []
    values_vector_relays = []
    time_vector_relays, values_vector_relays = vm.get_time_series(params_relays)

    params_bridges = {"query": "sum_over_time(count(bridge_bandwidth) by (day))[24h]"}
    time_vector_bridges = []
    values_vector_bridges = []
    time_vector_bridges, values_vector_bridges = vm.get_time_series(params_bridges)

    username = current_username()
    chart = (
        Chart()
        .plot(time_vector_relays, values_vector_relays, label="Relays")
        .plot(time_vector_bridges, values_vector_bridges, label="Bridges")
        .set_title("Server Metrics")
        .autofmt_xdate()
        .legend()
        .get_svg()
    )
    context = ServersResponseDTO(chart=chart, username=username, title="Server Metrics")
    return render_template("servers/index.html", **dataclasses.asdict(context))
