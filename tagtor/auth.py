"""
Auth logic
"""

import base64
import dataclasses

from flask import Blueprint, render_template, request

from tagtor.db import Database
from tagtor.dto.auth import AuthResponseDTO


bp = Blueprint("auth", __name__)


def current_username():
    """
    Returns the current username for the logged user
    """
    header_token = request.headers["Authorization"].split("Basic ")[1]
    auth_header = str.encode(header_token)
    username = base64.b64decode(auth_header).decode("utf-8").split(":")[0]
    return username


@bp.route("/auth")
def index():
    username = current_username()
    tags = Database.get_tags_by_username(username)
    notes = Database.get_notes_by_username(username)
    context = AuthResponseDTO(
        title="Tor nodes", username=username, tags=tags, notes=notes
    )
    return render_template("auth/index.html", **dataclasses.asdict(context))
