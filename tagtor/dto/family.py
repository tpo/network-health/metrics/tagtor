from dataclasses import dataclass

from tagtor.constants import SortDirection
from tagtor.models.status import ServerStatusModel
from tagtor.models.tags import ServerTagModel


@dataclass
class FamilyParamsDTO:
    offset: int
    limit: int
    sort_by: str


@dataclass
class FamilyResponseDTO:
    title: str
    routers: list[ServerStatusModel]
    sort: SortDirection
    params: FamilyParamsDTO
    tags: list[ServerTagModel]
    top_tags: list[str]
    digest: str
