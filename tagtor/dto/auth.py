from dataclasses import dataclass

from tagtor.models.note import ServerNoteModel
from tagtor.models.tags import ServerTagModel


@dataclass
class AuthResponseDTO:
    title: str
    username: str
    tags: list[ServerTagModel]
    notes: list[ServerNoteModel]
