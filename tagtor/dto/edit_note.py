from dataclasses import dataclass

from tagtor.models.note import ServerNoteModel


@dataclass
class EditNoteResponseDTO:
    title: str
    fingerprint: str
    note: ServerNoteModel
    username: str
