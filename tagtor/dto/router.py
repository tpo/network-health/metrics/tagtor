from dataclasses import dataclass

from tagtor.models.note import ServerNoteModel
from tagtor.models.status import ServerStatusModel
from tagtor.models.tags import ServerTagModel


@dataclass
class RouterResponseDTO:
    title: str
    router: ServerStatusModel
    tags: list[ServerTagModel]
    top_tags: list[str]
    notes: list[ServerNoteModel]
    network_fraction_chart: str
    network_bandwidth_chart: str
