from dataclasses import dataclass

@dataclass
class TrafficResponseDTO:
    title: str
    chart: str
    username: str
