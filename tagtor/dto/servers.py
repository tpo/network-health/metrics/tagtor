from dataclasses import dataclass


@dataclass
class ServersResponseDTO:
    chart: str
    username: str
    title: str
