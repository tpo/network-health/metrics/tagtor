from dataclasses import dataclass
from datetime import datetime


@dataclass
class FamiliesStatusModel:
    family_digest: str
    fingerprints: int
    published: datetime
    bandwidth: int
    advertised_bandwidth: int
    network_weight_fraction: int


@dataclass
class FamiliesParamsDTO:
    offset: int
    limit: int


@dataclass
class FamiliesResponseDTO:
    title: str
    routers: list[FamiliesStatusModel]
    params: FamiliesParamsDTO
