from dataclasses import dataclass

from tagtor.models.note import ServerNoteModel
from tagtor.models.tags import ServerTagModel


@dataclass
class CommentResponseDTO:
    title: str
    fingerprint: str
    notes: list[ServerNoteModel]
    tags: list[ServerTagModel]
