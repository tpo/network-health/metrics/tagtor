from dataclasses import dataclass

from tagtor.models.tags import ServerTagModel


@dataclass
class TagsResponseDTO:
    tags: list[ServerTagModel]
    title: str
    fingerprint: str
