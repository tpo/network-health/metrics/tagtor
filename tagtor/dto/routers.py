from dataclasses import dataclass

from tagtor.constants import SortDirection
from tagtor.models.status import ServerStatusModel


@dataclass
class RoutersParamsDTO:
    offset: int
    limit: int
    sort_by: str


@dataclass
class RoutersResponseDTO:
    title: str
    routers: list[ServerStatusModel]
    sort: SortDirection
    params: RoutersParamsDTO
