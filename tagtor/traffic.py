import dataclasses
from flask import (
    Blueprint, current_app, render_template
)

from tagtor.auth import current_username
from tagtor.chart import Chart
from tagtor.dto.traffic import TrafficResponseDTO
from tagtor.vm import VictoriaMetrics
from datetime import datetime

bp = Blueprint('traffic', __name__)


@bp.route('/traffic')
def index():
    vm = VictoriaMetrics()

    if current_app.config['REMOTE']:
        vm.authenticate()

    params_bw_hist = {
        "query": "sum(" +
                 "sum(" +
                 "sum_over_time(write_bandwidth_history{" +
                 "node=\"relay\"}[24h]))," +
                 "sum(" +
                 "sum_over_time(" +
                 "read_bandwidth_history{" +
                 "node=\"relay\"}[24h])))*8/2/10e8"
    }
    time_vector_bw_hist = []
    values_vector_bw_hist = []
    time_vector_bw_hist, \
        values_vector_bw_hist = vm.get_time_series(params_bw_hist)

    params_bw_adv = {
        "query": "sum(" +
                 "sum_over_time(" +
                 "(min" +
                 "(bw_file_desc_bw_avg, " +
                 "bw_file_desc_bw_bur, bw_file_desc_bw_obs_last)" +
                 " by (fingerprint, time)" +
                 "))[24h])*8/10e8"

    }
    time_vector_bw_adv = []
    values_vector_bw_adv = []
    time_vector_bw_adv, \
        values_vector_bw_adv = vm.get_time_series(params_bw_adv)

    username = current_username()
    chart = (
        Chart()
        .plot(time_vector_bw_hist, values_vector_bw_hist, label="Bandwidth History")
        .plot(time_vector_bw_adv, values_vector_bw_adv, label="Bandwidth Advertised")
        .set_title("Traffic Metrics")
        .autofmt_xdate()
        .legend()
        .get_svg()
    )
    context = TrafficResponseDTO(chart=chart, username=username, title="Traffic Metrics")
    return render_template('traffic/index.html', **dataclasses.asdict(context))
