"""
Time series database logic
"""

import json
import requests

from flask import current_app
from datetime import datetime
from requests.auth import HTTPBasicAuth

TIME_FORMAT = "%Y-%m-%d"


class VictoriaMetrics(object):

    def __init__(self):
        self.url = current_app.config["METRICS_DB_URL"]
        self.auth = ""
        self.step = "24h"
        self.start = "-30d"
        self.end = "-1d"
        self.nocache = "1"

    """
        Prepare auth object if needed
    """

    def authenticate(self):
        username = current_app.config["METRICS_DB_USER"]
        password = current_app.config["METRICS_DB_PASSWORD"]
        self.auth = HTTPBasicAuth(username, password)

    """
        Build parameters for query based on defaults
    """

    def build_params(self, params):
        if "start" not in params.keys():
            params["start"] = self.start
        if "end" not in params.keys():
            params["end"] = self.end
        if "step" not in params.keys():
            params["step"] = self.step
        if "nocache" not in params.keys():
            params["nocache"] = "1"

        return params

    """
        Get time series from VictoriaMetrics

        If authentication is needed pass auth opject in the request as
        requests.get(url, params, auth=self.auth)
    """

    def get_time_series(self, params):
        params = self.build_params(params)
        if current_app.config["REMOTE"]:
            response = requests.get(self.url, params, auth=self.auth)
        else:
            response = requests.get(self.url, params)
        data = json.loads(response.text)

        time_vector = []
        values_vector = []
        if data["data"]["result"]:
            for d in data["data"]["result"][0]["values"]:
                utc_timestamp = datetime.utcfromtimestamp(d[0])
                timestamp = utc_timestamp.strftime(TIME_FORMAT)
                d1 = datetime.strptime(timestamp + "+0000", TIME_FORMAT + "%z")
                time_vector.append("{}".format(d1))
                values_vector.append(float(d[1]))
        time_vector = [
            datetime.strptime(tv, "%Y-%m-%d %H:%M:%S%z") for tv in time_vector
        ]
        return time_vector, values_vector
