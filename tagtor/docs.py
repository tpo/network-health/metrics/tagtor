from flask import (
    Blueprint, render_template
)

from tagtor.auth import current_username

bp = Blueprint('docs', __name__)


@bp.route('/docs')
def index():
    username = current_username()

    context = {
        "title": "Docs",
        "username": username
    }
    return render_template('docs/index.html', **context)
