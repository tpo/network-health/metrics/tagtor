from logging.config import dictConfig
import os
import re


from flask import Flask, render_template

from tagtor.auth import current_username

from urllib.parse import quote as urlquote

from tagtor.db import Database


def create_app():
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    # Custom quote function to ensure forward slashes are encoded
    def forward_quote(s):
        return urlquote(str(s), safe="")

    def cleanup_string(value):
        return re.sub(r"[^a-zA-Z0-9]", "", value)

    # Register the custom filters with Jinja2
    app.jinja_env.filters["quote"] = forward_quote
    app.jinja_env.filters["cleanup_string"] = cleanup_string

    # Load default environment variables
    app.config.from_pyfile("default_config.py")
    # Load custom environment variables
    app.config.from_envvar("TAGTOR_CONFIG", silent=True)
    # Configure logging
    dictConfig(app.config["LOGGING"])
    app.secret_key = app.config["SECRET_KEY"]
    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    with app.app_context():
        Database.initialise()

    @app.route("/")
    def index():

        username = current_username()

        context = {"title": "Tor Metrics", "username": username}

        return render_template("index.html", **context)

    from . import auth, routers, servers, traffic, docs

    app.register_blueprint(routers.bp)
    app.add_url_rule("/routers", endpoint="index")

    app.register_blueprint(auth.bp)
    app.add_url_rule("/auth", endpoint="index")

    app.register_blueprint(servers.bp)
    app.add_url_rule("/servers", endpoint="index")

    app.register_blueprint(traffic.bp)
    app.add_url_rule("/traffic", endpoint="index")

    app.register_blueprint(docs.bp)
    app.add_url_rule("/docs", endpoint="index")

    return app
