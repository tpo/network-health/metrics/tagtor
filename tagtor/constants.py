from dataclasses import dataclass, field
from datetime import datetime
from enum import Enum
from typing import Optional


class SortDirection(str, Enum):
    ASC = "asc"
    DESC = "desc"


class TopTags(str, Enum):
    GUS_KNOWS = "gus-knows"
    ARMA_KNOWS = "arma-knows"
    GK_KNOWS = "gk-knows"
    CHAT = "chat"
    MAIL = "mail"
    IN_PERSON = "in-person"


@dataclass
class RouterFilters:
    is_bridge: Optional[bool] = field(default=None)
    published: Optional[datetime] = None
    nickname: Optional[str] = None
    fingerprint: Optional[str] = None
    or_addresses: Optional[list[str]] = None  # Check this one = None
    last_seen: Optional[datetime] = None
    first_seen: Optional[datetime] = None
    running: Optional[bool] = field(default=None)
    flags: Optional[list[str]] = None
    country: Optional[str] = None
    country_name: Optional[str] = None
    autonomous_system: Optional[str] = None
    as_name: Optional[str] = None
    verified_host_names: Optional[str] = None
    last_restarted: Optional[datetime] = None
    exit_policy: Optional[list[str]] = None
    contact: Optional[str] = None
    platform: Optional[str] = None
    version: Optional[str] = None
    version_status: Optional[str] = None
    effective_family: Optional[list[str]] = None
    declared_family: Optional[list[str]] = None
    transport: Optional[str] = None
    bridgedb_distributor: Optional[str] = None
    blocklist: Optional[str] = None
    last_changed_address_or_port: Optional[datetime] = None
    diff_or_addresses: Optional[list[int]] = None
    unverified_host_names: Optional[list[str]] = None
    unreachable_or_addresses: Optional[list[int]] = None
    overload_ratelimits_timestamp: Optional[datetime] = None
    overload_ratelimits_ratelimit: Optional[int] = None
    overload_ratelimits_burstlimit: Optional[int] = None
    overload_ratelimits_read_count: Optional[int] = None
    overload_ratelimits_write_count: Optional[int] = None
    overload_fd_exhausted_timestamp: Optional[int] = None
    overload_general_timestamp: Optional[datetime] = None
    family_digest: Optional[str] = None
    advertised_bandwidth: Optional[str] = None
    bandwidth: Optional[str] = None
    network_weight_fraction: Optional[str] = None
    bandwidth_rate: Optional[str] = None
    bandwidth_burst: Optional[str] = None
    bandwidth_observed: Optional[str] = None
    ipv6_default_policy: Optional[str] = None
    ipv6_port_list: Optional[str] = None
    socks_port: Optional[int] = None
    dir_port: Optional[int] = None
    or_port: Optional[int] = None
    is_hibernating: Optional[bool] = None
    tags: Optional[list[str]] = None
    tagged: Optional[bool] = None

    def __post_init__(self):
        self.is_bridge = self.is_bridge and self.is_bridge == "true"
        self.running = self.running and self.running == "true"
        if self.tagged:
            self.tagged = self.tagged == "true"
        if self.flags:
            if type(self.flags) is str:
                self.flags = [self.flags.capitalize()]
            else:
                self.flags = [flag.capitalize() for flag in self.flags]


valid_query_columns = [
    "is_bridge",
    "published",
    "nickname",
    "fingerprint",
    "or_addresses",
    "last_seen",
    "first_seen",
    "running",
    "flags",
    "country",
    "country_name",
    "autonomous_system",
    "as_name",
    "verified_host_names",
    "last_restarted",
    "exit_policy",
    "contact",
    "platform",
    "version",
    "version_status",
    "effective_family",
    "declared_family",
    "transport",
    "bridgedb_distributor",
    "blocklist",
    "last_changed_address_or_port",
    "diff_or_addresses",
    "unverified_host_names",
    "unreachable_or_addresses",
    "overload_ratelimits_timestamp",
    "overload_ratelimits_ratelimit",
    "overload_ratelimits_burstlimit",
    "overload_ratelimits_read_count",
    "overload_ratelimits_write_count",
    "overload_fd_exhausted_timestamp",
    "overload_general_timestamp",
    "family_digest",
    "advertised_bandwidth",
    "bandwidth",
    "network_weight_fraction",
    "bandwidth_rate",
    "bandwidth_burst",
    "bandwidth_observed",
    "ipv6_default_policy",
    "ipv6_port_list",
    "socks_port",
    "dir_port",
    "or_port",
    "is_hibernating",
]
