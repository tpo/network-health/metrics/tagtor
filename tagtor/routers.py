"""
App logic
"""

import dataclasses
from typing import cast
from flask import (
    Blueprint,
    current_app,
    flash,
    render_template,
    redirect,
    request,
    url_for,
)

from tagtor.auth import current_username
from tagtor.chart import Chart
from tagtor.constants import RouterFilters, SortDirection, TopTags
from tagtor.db import Database
from tagtor.dto.comments import CommentResponseDTO
from tagtor.dto.edit_note import EditNoteResponseDTO
from tagtor.dto.families import FamiliesParamsDTO, FamiliesResponseDTO
from tagtor.dto.family import FamilyParamsDTO, FamilyResponseDTO
from tagtor.dto.router import RouterResponseDTO
from tagtor.dto.routers import RoutersParamsDTO, RoutersResponseDTO
from tagtor.dto.tags import TagsResponseDTO
from tagtor.vm import VictoriaMetrics

from urllib.parse import unquote as urlunquote

bp = Blueprint("routers", __name__)

TIME_FORMAT = "%Y-%m-%d"


@bp.route("/routers")
def routers():
    args = request.args.to_dict()
    # Sorting args
    sort_by = args.pop("sort_by", "network_weight_fraction")
    sort_dir = args.pop("sort_dir", SortDirection.DESC)
    # Pagination args
    limit = int(args.pop("limit", 10))
    offset = int(args.pop("offset", 0))
    # Filter args
    filters = RouterFilters(**args)
    routers = Database.get_routers(filters, limit, offset, sort_by, sort_dir)
    if len(routers) < 1:
        routers = []
        flash("No more nodes to show.")

    context = RoutersResponseDTO(
        title="Tor nodes",
        routers=routers,
        sort=sort_dir,
        params=RoutersParamsDTO(offset=offset, limit=limit, sort_by=sort_by),
    )
    return render_template("routers/routers.html", **dataclasses.asdict(context))


@bp.route("/families")
def families():
    args = request.args.to_dict()
    # Pagination args
    limit = int(args.pop("limit", 10))
    offset = int(args.pop("offset", 0))
    # Filter args
    filters = RouterFilters(**args)
    routers = Database.get_families(filters, limit, offset)
    if len(routers) < 1:
        routers = []
        flash("No more families to show.")

    context = FamiliesResponseDTO(
        title="Tor families",
        routers=routers,
        params=FamiliesParamsDTO(offset=offset, limit=limit),
    )
    return render_template("routers/families.html", **dataclasses.asdict(context))


@bp.route("/routers/search")
def search():
    args = request.args.to_dict()
    query = args.pop("query", None)
    if query is None:
        return redirect(url_for("routers.routers"))
    return redirect(url_for("routers.routers") + f"?{query}")


@bp.route("/routers/<fingerprint>")
def router(fingerprint: str):
    router = Database.get_router(fingerprint)
    tags = []
    top_tags = set([tag.value for tag in TopTags])
    notes = []
    if router:
        digest = router.family_digest
        if digest:
            tags = Database.get_tags_by_family(digest)
        else:
            tags = Database.get_tags(fingerprint)
        notes = Database.get_notes(fingerprint)
        top_retrieved_tags = Database.get_top_tags(10)
        for tag in top_retrieved_tags:
            top_tags.add(tag.tag)

    else:
        flash("This router is not in our database.")

    if router:
        # Sanitize the exit addresses
        if router.exit_policy:
            router.exit_policy = (
                router.exit_policy.replace("[", "")
                .replace("]", "")
                .replace("\\", "")
                .replace('"', "")
            )
        # Sanitize the OR addresses
        if router.or_addresses:
            router.or_addresses = router.or_addresses[1:-1]
            router.or_addresses = ",".join(
                [address[1:-1] for address in router.or_addresses.split(",")]
            )

    vm = VictoriaMetrics()

    if current_app.config["REMOTE"]:
        vm.authenticate()

    params_bw_read_hist = {
        "query": "read_bandwidth_history{fingerprint='" + fingerprint + "'}"
    }

    time_vector_bw_read = []
    values_vector_bw_read = []

    time_vector_bw_read, values_vector_bw_read = vm.get_time_series(params_bw_read_hist)

    time_vector_bw_write = []
    values_vector_bw_write = []

    params_bw_write_hist = {
        "query": "write_bandwidth_history{fingerprint='" + fingerprint + "'}"
    }

    time_vector_bw_write, values_vector_bw_write = vm.get_time_series(
        params_bw_write_hist
    )

    time_vector_exit = []
    values_vector_exit = []
    time_vector_middle = []
    values_vector_middle = []
    time_vector_guard = []
    values_vector_guard = []

    """
        If router is not a bridge compute the network fractions

        The first field in the status table is a boolean value which stores
        `True` when the node is a bridge.
    """
    if not router.is_bridge:
        params_exit_fraction = {
            "query": "network_exit_fraction{fingerprint='" + fingerprint + "'}"
        }
        params_guard_fraction = {
            "query": "network_guard_fraction{fingerprint='" + fingerprint + "'}"
        }
        params_middle_fraction = {
            "query": "network_middle_fraction{fingerprint='" + fingerprint + "'}"
        }

        time_vector_exit, values_vector_exit = vm.get_time_series(params_exit_fraction)
        time_vector_guard, values_vector_guard = vm.get_time_series(
            params_guard_fraction
        )
        time_vector_middle, values_vector_middle = vm.get_time_series(
            params_middle_fraction
        )

    network_bandwidth_chart = (
        Chart()
        .plot(time_vector_bw_read, values_vector_bw_read, label="Read bandwidth")
        .plot(time_vector_bw_write, values_vector_bw_write, label="Written bandwidth")
        .autofmt_xdate()
        .legend()
        .get_svg()
    )

    network_fraction_chart = (
        Chart()
        .plot(time_vector_exit, values_vector_exit, label="Network Exit Fraction")
        .plot(time_vector_guard, values_vector_guard, label="Network Guard Fraction")
        .plot(time_vector_middle, values_vector_middle, label="Network Middle Fraction")
        .autofmt_xdate()
        .legend()
        .get_svg()
    )

    context = RouterResponseDTO(
        title=fingerprint,
        router=router,
        tags=tags,
        top_tags=list(top_tags),
        notes=notes,
        network_fraction_chart=network_fraction_chart,
        network_bandwidth_chart=network_bandwidth_chart,
    )
    return render_template("routers/router.html", **dataclasses.asdict(context))


@bp.route("/routers/<fingerprint>/notes", methods=["POST", "GET"])
def notes(fingerprint: str):
    if request.method == "POST":
        username = current_username()
        note = request.form["note"]
        if not note:
            flash("note is required!")
        else:
            try:
                Database.add_note(fingerprint, username, note)
            except Exception as e:
                current_app.logger.error(e)
                flash("Database error. Is this note already present?")
        return redirect(url_for("routers.router", fingerprint=fingerprint))

    if request.method == "GET":
        username = current_username()
        router = Database.get_router(fingerprint)
        notes = []
        if router:
            notes = Database.get_notes(fingerprint)
        else:
            flash("This router is not in our database.")
        context = {
            "title": "notes activity",
            "fingerprint": fingerprint,
            "notes": notes,
            "username": username,
        }
        return render_template("routers/notes.html", **context)


@bp.route("/routers/<fingerprint>/notes/<note_id>/edit", methods=["POST", "GET"])
def edit_note(fingerprint: str, note_id: str):
    if request.method == "GET":
        username = current_username()
        router = Database.get_router(fingerprint)
        note = None
        if router:
            note = Database.get_note(note_id)
        else:
            flash(
                "Either this router is not in our database or we cannot\
                  find this note."
            )
        context = EditNoteResponseDTO(
            title="edit note",
            fingerprint=fingerprint,
            note=note,
            username=username,
        )
        return render_template("routers/edit_note.html", **dataclasses.asdict(context))

    if request.method == "POST":
        username = current_username()
        req_note = cast(str, request.form["note"])
        if not note:
            flash("note is required!")
        try:
            Database.edit_note(fingerprint, username, note_id, req_note, "edited")
        except Exception as e:
            current_app.logger.error(e)
            flash("Database error.")
    return redirect(url_for("routers.router", fingerprint=fingerprint))


@bp.route("/routers/<fingerprint>/notes/<note_id>/delete", methods=["POST"])
def delete_note(fingerprint: str, note_id: str):
    if request.method == "POST":
        username = current_username()
        try:
            Database.delete_note(fingerprint, username, note_id)
        except Exception as e:
            current_app.logger.error(e)
            flash("Database error.")
    return redirect(url_for("routers.router", fingerprint=fingerprint))


@bp.route("/routers/<fingerprint>/notes/<note_id>/restore", methods=["POST"])
def restore_note(fingerprint: str, note_id: str):
    if request.method == "POST":
        username = current_username()
        try:
            Database.restore_note(fingerprint, username, note_id)
        except Exception as e:
            current_app.logger.error(e)
            flash("Database error.")
    return redirect(url_for("routers.router", fingerprint=fingerprint))


@bp.route("/routers/family/<digest>/tags", methods=["POST"])
def family_tags(digest: str):
    digest = urlunquote(digest)
    if request.method == "POST":
        req_tags = request.form.get("tag")
        req_freq_tags = request.form.get("frequentTags")

        tags = []
        freq_tags = []
        if req_tags:
            tags = [req_tags]
        if req_freq_tags:
            freq_tags = req_freq_tags.split(",")

        if not (tags or freq_tags):
            flash("Tag is required!")
        else:
            fingerprint = cast(str, request.form.get("fingerprint"))
            username = current_username()
            if freq_tags:
                tags += freq_tags
            value = Database.add_tags(fingerprint, username, tags, digest)
            if value is not None:
                flash("Database error. Is this tag already present?")

        return redirect(url_for("routers.family", digest=digest))


@bp.route("/routers/<fingerprint>/tags", methods=["POST", "GET"])
def tags(fingerprint: str):

    if request.method == "POST":
        req_tags = request.form.get("tag")
        req_freq_tags = request.form.get("frequentTags")

        tags = []
        freq_tags = []
        if req_tags:
            tags = [req_tags]
        if req_freq_tags:
            freq_tags = req_freq_tags.split(",")

        if not (tags or freq_tags):
            flash("Tag is required!")
        else:
            username = current_username()
            digest = request.form.get("digest")
            if digest:
                digest = urlunquote(digest)
            if freq_tags:
                tags += freq_tags
            value = Database.add_tags(fingerprint, username, tags, digest)
            if value is not None:
                flash("Database error. Is this tag already present?")

        return redirect(url_for("routers.router", fingerprint=fingerprint))

    if request.method == "GET":
        router = Database.get_router(fingerprint)
        res_tags = []
        if router:
            res_tags = Database.get_tags(fingerprint)
        else:
            flash("This router is not in our database.")
        context = TagsResponseDTO(
            tags=res_tags,
            title="Tags Activity",
            fingerprint=fingerprint,
        )
        return render_template("routers/tags.html", **dataclasses.asdict(context))


@bp.route("/routers/<fingerprint>/tags/<tag>/delete", methods=["POST"])
def delete_tag(fingerprint: str, tag: str):
    if request.method == "POST":
        username = current_username()
        try:
            Database.set_tag_status(fingerprint, username, tag, "deleted")
        except Exception as e:
            current_app.logger.error(e)
            flash("Database error. Is this tag already present?")
    return redirect(url_for("routers.router", fingerprint=fingerprint))


@bp.route("/routers/<fingerprint>/tags/<tag>/restore", methods=["POST"])
def restore_tag(fingerprint: str, tag: str):
    if request.method == "POST":
        username = current_username()
        value = Database.set_tag_status(fingerprint, username, tag, "published")
        if value is not None:
            flash("Database error. Is this tag already present?")
    return redirect(url_for("routers.router", fingerprint=fingerprint))


@bp.route("/routers/<fingerprint>/comments", methods=["GET"])
def comments(fingerprint: str):
    if request.method == "GET":
        router = Database.get_router(fingerprint)
        notes = []
        tags = []
        if router:
            notes = Database.get_notes(fingerprint)
            tags = Database.get_tags(fingerprint)
        else:
            flash("This router is not in our database.")
        context = CommentResponseDTO(
            title="comments activity",
            fingerprint=fingerprint,
            notes=notes,
            tags=tags,
        )
        return render_template("routers/comments.html", **dataclasses.asdict(context))
