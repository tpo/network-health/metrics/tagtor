import base64
import io
from matplotlib import pyplot as plt


class Chart(object):
    """
    Create a matplotlib plot & export it to be used in Jinja templates
    """
    def __init__(self):
        """
        Initialise the plot with the backend config
        """
        plt.switch_backend("Agg")
        self.fig, self.ax = plt.subplots()

    def plot(self, *args, **kwargs):
        """
        Plots the line chart
        """
        self.ax.plot(*args, **kwargs)
        return self
      
    def bar(self, *args, **kwargs):
        """
        Plots the bar chart
        """
        self.ax.bar(*args, **kwargs)
        return self

    def set_xlabel(self, *args, **kwargs):
        """
        Set the x axis label
        """
        self.ax.set_xlabel(*args, **kwargs)
        return self

    def set_ylabel(self, *args, **kwargs):
        """
        Set the y axis label
        """
        self.ax.set_ylabel(*args, **kwargs)
        return self

    def set_title(self, *args, **kwarg):
        """
        Set the chart title
        """
        self.ax.set_title(*args, **kwarg)
        return self

    def legend(self, *args, **kwargs):
        """
        Set the legend
        """
        self.ax.legend(*args, **kwargs)
        return self

    def autofmt_xdate(self):
        """
        Format the x axis ticks
        """
        self.fig.autofmt_xdate()
        return self

    def get_png(self) -> str:
        """
        Get the chart in png format

        Returns:
            str: Base64 encoded PNG representation of the chart
        """
        img = io.BytesIO()
        self.fig.savefig(img, format='png')
        plt.close(self.fig)
        img.seek(0)
        return base64.b64encode(img.getvalue()).decode('utf8')

    def get_svg(self) -> str:
        """
        Get the chart in svg format

        Returns:
            str: SVG representation of the chart
        """
        img = io.BytesIO()
        self.fig.savefig(img, format='svg', bbox_inches='tight')
        plt.close(self.fig)
        img.seek(0)
        return img.getvalue().decode('utf8')
