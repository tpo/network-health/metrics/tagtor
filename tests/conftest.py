"""
Tests config
"""

import os

import pytest

from tagtor import create_app
from tagtor.db import Database

with open(os.path.join(os.path.dirname(__file__), "data.sql"), "rb") as f:
    _data_sql = f.read().decode("utf8")

with open(os.path.join(os.path.dirname(__file__), "schema.sql"), "rb") as f:
    _schema_sql = f.read().decode("utf8")


@pytest.fixture
def app():

    app = create_app()

    with app.app_context():
        with Database.execute() as cursor:
            assert cursor is not None
            cursor.execute(_schema_sql)
            cursor.execute(_data_sql)
    yield app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()
