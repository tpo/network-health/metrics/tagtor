/**
 * SQL schema
 */
DROP TABLE IF EXISTS server_status CASCADE;
DROP TABLE IF EXISTS server_tag;


CREATE TABLE IF NOT EXISTS server_status(
  is_bridge                           BOOLEAN                      NOT NULL,
  published                           TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  nickname                            TEXT                         NOT NULL,
  fingerprint                         TEXT                         NOT NULL,
  or_addresses                        TEXT,
  last_seen                           TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  first_seen                          TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  running                             BOOLEAN,
  flags                               TEXT,
  country                             TEXT,
  country_name                        TEXT,
  autonomous_system                   TEXT,
  as_name                             TEXT,
  verified_host_names                 TEXT,
  last_restarted                      TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  exit_policy                         TEXT,
  contact                            TEXT,
  platform                            TEXT,
  version                             TEXT,
  version_status                      TEXT,
  effective_family                    TEXT,
  declared_family                     TEXT,
  transport                           TEXT,
  bridgedb_distributor                TEXT,
  blocklist                           TEXT,
  last_changed_address_or_port        TIMESTAMP WITHOUT TIME ZONE,
  diff_or_addresses                   TEXT,
  unverified_host_names               TEXT,
  unreachable_or_addresses            TEXT,
  overload_ratelimits_version         INTEGER,
  overload_ratelimits_timestamp       TIMESTAMP WITHOUT TIME ZONE,
  overload_ratelimits_ratelimit       BIGINT,
  overload_ratelimits_burstlimit      BIGINT,
  overload_ratelimits_read_count      BIGINT,
  overload_ratelimits_write_count     BIGINT,
  overload_fd_exhausted_version       INTEGER,
  overload_fd_exhausted_timestamp     TIMESTAMP WITHOUT TIME ZONE,
  overload_general_timestamp          TIMESTAMP WITHOUT TIME ZONE,
  overload_general_version            INTEGER,
  first_network_status_digest         TEXT,
  first_network_status_entry_digest   TEXT,
  last_network_status_digest          TEXT,
  last_network_status_entry_digest    TEXT,
  first_bridge_network_status_digest         TEXT,
  first_bridge_network_status_entry_digest   TEXT,
  last_bridge_network_status_digest          TEXT,
  last_bridge_network_status_entry_digest    TEXT,
  last_network_status_weights_id      uuid,
  last_network_status_totals_id       uuid,
  last_server_descriptor_digest       TEXT,
  last_extrainfo_descriptor_digest    TEXT,
  last_bridgestrap_stats_digest       TEXT,
  last_bridgestrap_test_digest        TEXT,
  last_bridge_pool_assignments_file_digest  TEXT,
  last_bridge_pool_assignment_digest  TEXT,
  family_digest                       TEXT,
  advertised_bandwidth                BIGINT,
  bandwidth                           BIGINT,
  network_weight_fraction             DOUBLE PRECISION,
  PRIMARY KEY(fingerprint, nickname, published)
);

CREATE TABLE IF NOT EXISTS server_tag(
  tag                               TEXT                         NOT NULL,
  username                          TEXT                         NOT NULL,
  published                         TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  edited                            TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  fingerprint                       TEXT                         NOT NULL,
  status                            TEXT                         NOT NULL,
  digest                            TEXT,
PRIMARY KEY(tag, fingerprint)
);

CREATE TABLE IF NOT EXISTS server_note(
  note_id                           BIGINT GENERATED ALWAYS AS IDENTITY,
  note                              TEXT                         NOT NULL,
  username                          TEXT                         NOT NULL,
  published                         TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  edited                            TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  fingerprint                       TEXT                         NOT NULL,
  status                            TEXT                         NOT NULL,
PRIMARY KEY(note_id, fingerprint)
);
