"""
App tests
"""


def test_index(client):
    response = client.get("/routers")
    assert b"22F74E176F803499D4F80D9CE7D325883A8C0E45" in response.data


def test_profile(client):
    response = client.get("/routers/22F74E176F803499D4F80D9CE7D325883A8C0E45")
    assert b"22F74E176F803499D4F80D9CE7D325883A8C0E45" in response.data


def test_tags(client):
    response = client.post(
        "/routers/22F74E176F803499D4F80D9CE7D325883A8C0E45/tags",
        data={
            "tag": "new-router-tag",
            "digest": "jCnYBmSw3PQ%252FFHV8rL2WsX74JrFGVOsGxBxyLNRDfzg",
        },
        environ_base={"HTTP_AUTHORIZATION": "Basic bWV0cmljczpwYXNzd29yZA=="},
    )
    assert response.status_code == 302
    response = client.get("/routers/22F74E176F803499D4F80D9CE7D325883A8C0E45")
    assert b"new-router-tag" in response.data


def test_delete_tag(client):
    tags_url = "/routers/22F74E176F803499D4F80D9CE7D325883A8C0E45/tags"
    response = client.post(
        tags_url,
        data={
            "tag": "del-hiro-knows",
            "digest": "jCnYBmSw3PQ%252FFHV8rL2WsX74JrFGVOsGxBxyLNRDfzg",
        },
        environ_base={"HTTP_AUTHORIZATION": "Basic bWV0cmljczpwYXNzd29yZA=="},
    )
    response = client.get(tags_url)
    assert b'<i class="fa-solid fa-trash-can"></i>' in response.data
    delete_tag_url = tags_url + "/del-hiro-knows/delete"
    response = client.post(
        delete_tag_url,
        environ_base={"HTTP_AUTHORIZATION": "Basic bWV0cmljczpwYXNzd29yZA=="},
    )
    assert response.status_code == 302
    response = client.get(tags_url)
    assert b'<i class="fa-solid fa-rotate-left"></i>' in response.data


def test_restore_tag(client):
    tags_url = "/routers/22F74E176F803499D4F80D9CE7D325883A8C0E45/tags"
    response = client.post(
        tags_url,
        data={
            "tag": "res-hiro-knows",
            "digest": "jCnYBmSw3PQ%252FFHV8rL2WsX74JrFGVOsGxBxyLNRDfzg",
        },
        environ_base={"HTTP_AUTHORIZATION": "Basic bWV0cmljczpwYXNzd29yZA=="},
    )
    delete_tag_url = tags_url + "/res-hiro-knows/delete"
    response = client.post(
        delete_tag_url,
        environ_base={"HTTP_AUTHORIZATION": "Basic bWV0cmljczpwYXNzd29yZA=="},
    )
    restore_tag_url = tags_url + "/res-hiro-knows/restore"
    response = client.post(
        restore_tag_url,
        environ_base={"HTTP_AUTHORIZATION": "Basic bWV0cmljczpwYXNzd29yZA=="},
    )
    assert response.status_code == 302
    response = client.get(tags_url)
    assert b'<i class="fa-solid fa-trash-can"></i>' in response.data


def test_tags_activity(client):
    response = client.post(
        "/routers/22F74E176F803499D4F80D9CE7D325883A8C0E45/tags",
        data={
            "tag": "published",
            "digest": "jCnYBmSw3PQ%252FFHV8rL2WsX74JrFGVOsGxBxyLNRDfzg",
        },
        environ_base={"HTTP_AUTHORIZATION": "Basic bWV0cmljczpwYXNzd29yZA=="},
    )
    assert response.status_code == 302
    response = client.get("/routers/22F74E176F803499D4F80D9CE7D325883A8C0E45/tags")
    assert b'<th scope="row">\n              published</th>\n' in response.data


def test_notes(client):
    response = client.post(
        "/routers/22F74E176F803499D4F80D9CE7D325883A8C0E45/notes",
        data={"note": "published by metrics"},
        environ_base={"HTTP_AUTHORIZATION": "Basic bWV0cmljczpwYXNzd29yZA=="},
    )
    assert response.status_code == 302
    response = client.get(
        "/routers/22F74E176F803499D4F80D9CE7D325883A8C0E45",
        environ_base={"HTTP_AUTHORIZATION": "Basic bWV0cmljczpwYXNzd29yZA=="},
    )
    assert b"published by metrics" in response.data


def test_notes_activity(client):
    response = client.post(
        "/routers/22F74E176F803499D4F80D9CE7D325883A8C0E45/notes",
        data={"note": "published by metrics again"},
        environ_base={"HTTP_AUTHORIZATION": "Basic bWV0cmljczpwYXNzd29yZA=="},
    )
    assert response.status_code == 302
    response = client.get(
        "/routers/22F74E176F803499D4F80D9CE7D325883A8C0E45/notes",
        environ_base={"HTTP_AUTHORIZATION": "Basic bWV0cmljczpwYXNzd29yZA=="},
    )
    assert b"published by metrics again" in response.data


def search(client):
    response = client.get(
        "/routers?contact=MakeSecure",
        environ_base={"HTTP_AUTHORIZATION": "Basic bWV0cmljczpwYXNzd29yZA=="},
    )
    assert response.status_code == 200
    assert b"<td>MakeSecure</td>" in response.data


def search_tags(client):
    response = client.post(
        "/routers/22F74E176F803499D4F80D9CE7D325883A8C0E45/tags",
        data={"tag": "in-person"},
        environ_base={"HTTP_AUTHORIZATION": "Basic bWV0cmljczpwYXNzd29yZA=="},
    )
    assert response.status_code == 302
    response = client.get("/routers/22F74E176F803499D4F80D9CE7D325883A8C0E45/tags")
    assert b'<th scope="row">\n              in-person</th>\n' in response.data
    response = client.get(
        "routers?tags=in-person",
        environ_base={"HTTP_AUTHORIZATION": "Basic bWV0cmljczpwYXNzd29yZA=="},
    )
    assert response.status_code == 200
    assert b"<td>MakeSecure</td>" in response.data
