"""
Database tests
"""

from tagtor.db import Database


def test_connect(app):
    with app.app_context():
        with Database.execute() as cursor:
            assert cursor is not None


def test_create_schema(app):
    with app.app_context():
        router = Database.get_router("00B5DE0522A6F215BF694DE36576AF1A3927D6D8")
        assert router.fingerprint == "00B5DE0522A6F215BF694DE36576AF1A3927D6D8"
