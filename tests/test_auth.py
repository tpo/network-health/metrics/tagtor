"""
Auth tests
"""


def test_index(client):
    response = client.get(
        "/auth", environ_base={"HTTP_AUTHORIZATION": "Basic bWV0cmljczpwYXNzd29yZA=="}
    )
    assert b"Recent updates for @metrics" in response.data


def test_activity_log(client):
    response = client.post(
        "/routers/22F74E176F803499D4F80D9CE7D325883A8C0E45/tags",
        data={
            "tag": "online",
            "digest": "jCnYBmSw3PQ%252FFHV8rL2WsX74JrFGVOsGxBxyLNRDfzg",
        },
        environ_base={"HTTP_AUTHORIZATION": "Basic bWV0cmljczpwYXNzd29yZA=="},
    )
    assert response.status_code == 302
    response = client.get(
        "/auth", environ_base={"HTTP_AUTHORIZATION": "Basic bWV0cmljczpwYXNzd29yZA=="}
    )
    assert b"22F74E176F803499D4F80D9CE7D325883A8C0E45" in response.data
    # assert b"online" in response.data
