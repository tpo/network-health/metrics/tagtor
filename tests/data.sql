/**
 * SQL test data
 */

INSERT INTO server_status (is_bridge, published, nickname, fingerprint,
  or_addresses, last_seen, first_seen, running, flags, country,
  country_name, autonomous_system, as_name, verified_host_names,
  last_restarted, exit_policy, contact, platform, version,
  version_status, effective_family, transport, bridgedb_distributor, blocklist,
  last_changed_address_or_port, diff_or_addresses, unverified_host_names,
  unreachable_or_addresses, overload_ratelimits_version,
  overload_ratelimits_timestamp, overload_ratelimits_ratelimit,
  overload_ratelimits_burstlimit, overload_ratelimits_read_count,
  overload_ratelimits_write_count, overload_fd_exhausted_version,
  overload_fd_exhausted_timestamp, overload_general_timestamp,
  overload_general_version,
  first_network_status_digest,
  first_network_status_entry_digest,
  last_network_status_digest,
  last_network_status_entry_digest,
  first_bridge_network_status_digest,
  first_bridge_network_status_entry_digest,
  last_bridge_network_status_digest,
  last_bridge_network_status_entry_digest,
  last_network_status_weights_id,
  last_network_status_totals_id,
  last_server_descriptor_digest,
  last_extrainfo_descriptor_digest,
  last_bridgestrap_stats_digest,
  last_bridgestrap_test_digest,
  last_bridge_pool_assignments_file_digest,
  last_bridge_pool_assignment_digest,
  family_digest, advertised_bandwidth, bandwidth, network_weight_fraction
) VALUES(
 't', '2022-08-30 20:54:49', 'wolfTest359', '00B5DE0522A6F215BF694DE36576AF1A3927D6D8',
 '["[fd9f:2e19:3bcf::b3:5224]:61528"]', '2022-08-31 11:54:43', '2022-08-31 11:54:43',
 't', '', '', '', '', '', null, '2022-08-25 23:02:27', '["reject *:*"]',
 'zantimisfit@protonmail.me', 'Tor 0.4.7.8 on OpenBSD', 'tor 0.4.7.8',
 'unrecommended', '','', 'email', '', '1969-12-31 23:59:59.999', '', null,
 '["[fd9f:2e19:3bcf::b3:5224]:61528"]', 0, '1969-12-31 23:59:59.999', -1, -1, -1, -1, 0, '1969-12-31 23:59:59.999',
 '1969-12-31 23:59:59.999', 0,
 null, null, null, null,
 'EoCh76XYZkmH2sbQoRNIgz7hMbJAvFR6WXsbf8LGe2g',
 'EL94HRN/+Ok/7sEn8BKJiTqZdMzK/Y4SbpBqsqPMBow',
 'EoCh76XYZkmH2sbQoRNIgz7hMbJAvFR6WXsbf8LGe2g', 'EL94HRN/+Ok/7sEn8BKJiTqZdMzK/Y4SbpBqsqPMBow',
 null, null,
 'CC649381F91CFA80B2A246D658708BAFD333192A',
 null, null, null, null, null, null, 4678893, 0, 0
);

INSERT INTO server_status (is_bridge, published, nickname, fingerprint,
  or_addresses, last_seen, first_seen, running, flags, country,
  country_name, autonomous_system, as_name, verified_host_names,
  last_restarted, exit_policy, contact, platform, version,
  version_status, effective_family, declared_family, transport,
  bridgedb_distributor, blocklist,
  last_changed_address_or_port, diff_or_addresses, unverified_host_names,
  unreachable_or_addresses, overload_ratelimits_version,
  overload_ratelimits_timestamp, overload_ratelimits_ratelimit,
  overload_ratelimits_burstlimit, overload_ratelimits_read_count,
  overload_ratelimits_write_count, overload_fd_exhausted_version,
  overload_fd_exhausted_timestamp, overload_general_timestamp,
  overload_general_version,
  first_network_status_digest,
  first_network_status_entry_digest,
  last_network_status_digest,
  last_network_status_entry_digest,
  first_bridge_network_status_digest,
  first_bridge_network_status_entry_digest,
  last_bridge_network_status_digest,
  last_bridge_network_status_entry_digest,
  last_network_status_weights_id,
  last_network_status_totals_id,
  last_server_descriptor_digest,
  last_extrainfo_descriptor_digest,
  last_bridgestrap_stats_digest,
  last_bridgestrap_test_digest,
  last_bridge_pool_assignments_file_digest,
  last_bridge_pool_assignment_digest,
  family_digest, advertised_bandwidth, bandwidth, network_weight_fraction
) VALUES(
  'f', '2022-08-31 11:01:05', 'MakeSecure', '22F74E176F803499D4F80D9CE7D325883A8C0E45',
  '[]', '2022-08-30 17:01:00', '2022-08-30 17:01:00', 't', '["Exit","Fast","Running","Stable","Valid"]',
  'nl', 'Netherlands', 'AS35470', 'Signet B.V.', null,
  '2022-08-30 17:00:00',
  '["reject 0.0.0.0/8:*","reject 169.254.0.0/16:*","reject 127.0.0.0/8:*","reject 192.168.0.0/16:*","reject 10.0.0.0/8:*","reject 172.16.0.0/12:*","reject 83.96.213.63:*","accept *:20-21","accept *:43","accept *:53","accept *:79","accept *:80-81","accept *:88","accept *:110","accept *:143","accept *:194","accept *:220","accept *:389","accept *:443","accept *:464","accept *:465","accept *:531","accept *:543-544","accept *:554","accept *:563","accept *:587","accept *:636","accept *:706","accept *:749","accept *:853","accept *:873","accept *:902-904","accept *:981","accept *:989-990","accept *:991","accept *:992","accept *:993","accept *:994","accept *:995","accept *:1194","accept *:1220","accept *:1293","accept *:1500","accept *:1533","accept *:1677","accept *:1723","accept *:1755","accept *:1863","accept *:2082","accept *:2083","accept *:2086-2087","accept *:2095-2096","accept *:2102-2104","accept *:3128","accept *:3389","accept *:3690","accept *:4321","accept *:4643","accept *:5050","accept *:5190","accept *:5222-5223","accept *:5228","accept *:5900","accept *:6660-6669","accept *:6679","accept *:6697","accept *:8000","accept *:8008","accept *:8074","accept *:8080","accept *:8082","accept *:8087-8088","accept *:8232-8233","accept *:8332-8333","accept *:8443","accept *:8888","accept *:9418","accept *:9999","accept *:10000","accept *:11371","accept *:19294","accept *:19638","accept *:50002","accept *:64738","reject *:*"]',
  'MakeSecure Tor Exit Abuse <tor-abuse -AT- makesecure -DOT- nl>', 'Tor 0.4.7.10 on Linux', 'Tor 0.4.7.10',
  'experimental', '22F74E176F803499D4F80D9CE7D325883A8C0E45', '22F74E176F803499D4F80D9CE7D325883A8C0E45', '',
  '', '', '2022-08-31 11:01:05', '[]', null, '', 0,  '1969-12-31 23:59:59.999', -1, -1, -1, -1, 0,
  '1969-12-31 23:59:59.999', '1969-12-31 23:59:59.999', 0,
  'i1ebQFUF3g8a7rxN4PVmuDicVqz1usJCM2LOWIiRMao', '3Q3BWfcKEw9MNwKdw6SXQQvm1/nc5e+kxwDwjwEQ3KQ',
  'i1ebQFUF3g8a7rxN4PVmuDicVqz1usJCM2LOWIiRMao', '3Q3BWfcKEw9MNwKdw6SXQQvm1/nc5e+kxwDwjwEQ3KQ',
  null, null, null, null,
  'b5a0026e-f36c-4eb3-b4b7-68fa55d45467', '0e713ee4-1b70-4055-be72-32b643ad06d9',
  '4dc9c214092201f889e5d7654e602a7489ec35af',
  null, null, null, null, null,
  'jCnYBmSw3PQ/FHV8rL2WsX74JrFGVOsGxBxyLNRDfzg', 1048576, 590, 4.4747912397724576e-07);

CREATE MATERIALIZED VIEW IF NOT EXISTS server_status_latest AS
SELECT DISTINCT ON (server_status.fingerprint)
    server_status.is_bridge,
    server_status.published,
    server_status.nickname,
    server_status.fingerprint,
    server_status.last_seen,
    server_status.first_seen,
    server_status.last_restarted,
    server_status.or_addresses,
    server_status.running,
    server_status.flags,
    server_status.country,
    server_status.country_name,
    server_status.autonomous_system,
    server_status.as_name,
    server_status.verified_host_names,
    server_status.exit_policy,
    server_status.contact,
    server_status.platform,
    server_status.version,
    server_status.version_status,
    server_status.effective_family,
    server_status.declared_family,
    server_status.transport,
    server_status.bridgedb_distributor,
    server_status.blocklist,
    server_status.last_changed_address_or_port,
    server_status.diff_or_addresses,
    server_status.unverified_host_names,
    server_status.unreachable_or_addresses,
    server_status.overload_ratelimits_version,
    server_status.overload_ratelimits_timestamp,
    server_status.overload_ratelimits_ratelimit,
    server_status.overload_ratelimits_burstlimit,
    server_status.overload_ratelimits_read_count,
    server_status.overload_ratelimits_write_count,
    server_status.overload_fd_exhausted_version,
    server_status.overload_fd_exhausted_timestamp,
    server_status.overload_general_timestamp,
    server_status.overload_general_version,
    server_status.first_network_status_digest,
    server_status.first_network_status_entry_digest,
    server_status.last_network_status_digest,
    server_status.last_network_status_entry_digest,
    server_status.first_bridge_network_status_digest,
    server_status.first_bridge_network_status_entry_digest,
    server_status.last_bridge_network_status_digest,
    server_status.last_bridge_network_status_entry_digest,
    server_status.last_network_status_weights_id,
    server_status.last_network_status_totals_id,
    server_status.last_server_descriptor_digest,
    server_status.last_extrainfo_descriptor_digest,
    server_status.last_bridgestrap_stats_digest,
    server_status.last_bridgestrap_test_digest,
    server_status.last_bridge_pool_assignments_file_digest,
    server_status.last_bridge_pool_assignment_digest,
    server_status.family_digest,
    server_status.advertised_bandwidth,
    server_status.bandwidth,
    server_status.network_weight_fraction
FROM server_status
ORDER BY server_status.fingerprint, server_status.published DESC
WITH DATA;