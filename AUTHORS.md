# Authors

The following people have contributed to tagtor. Thank
you for helping make Tor better.

Georg Koppen [GeKo]
Juga [juga]
Silvia Puglisi [hiro]

Last updated: 2024-06-05 on aec69ca1
